/*
 * Copyright Philippe Steinmann 2020 - 2020.
 * Distributed under the Boost Software License, Version 1.0.
 * (See accompanying file LICENSE.txt or copy at
 * https://www.boost.org/LICENSE_1_0.txt)
 */
#ifndef MDT_UIC_NUMBER_STRING_FORMAT_VALIDATION_STATE_QT_H
#define MDT_UIC_NUMBER_STRING_FORMAT_VALIDATION_STATE_QT_H

#include "Mdt/UicNumber/StringFormatValidationState.h"
#include <QString>
#include <QCoreApplication>

namespace Mdt{ namespace UicNumber{

  /*! \brief Get a generic error message string from \a errorCode
   *
   * \sa errorMessageString()
   */
  inline
  QString errorMessageQString(StringFormatValidationErrorCode errorCode)
  {
    switch(errorCode){
      case StringFormatValidationErrorCode::NoError:
        return QString();
      case StringFormatValidationErrorCode::WrongDigitCount:
        return QCoreApplication::translate("Mdt::UicNumber::errorMessageQString()", "The count of digits is wrong (allowed are 11 or 12 digits");
      case StringFormatValidationErrorCode::UnallowedChar:
        return QCoreApplication::translate("Mdt::UicNumber::errorMessageQString()", "At least one unallowed char is present in the string");
    }
    return QString();
  }

}} // namespace Mdt{ namespace UicNumber{

#endif // #ifndef MDT_UIC_NUMBER_STRING_FORMAT_VALIDATION_STATE_QT_H
